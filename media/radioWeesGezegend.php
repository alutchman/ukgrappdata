<?php
header("Cache-Control: no-store, no-cache, must-revalidate"); // HTTP/1.1
header("Cache-Control: post-check=0, pre-check=0", false);
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
header("Pragma: no-cache"); // HTTP/1.0
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, height=device-height; initial-scale=1.0,maximum-scale=1, user-scalable=0 ">
    <title>Zekerheid en Zegen</title>
    <style>
        body {
            height : 100%;
            background: url("../images/radiopositief.jpg") no-repeat center center fixed;
            background-size: auto;

            font-family: Arial, Helvetica, sans-serif;
            color: #000000;
        }

        h1 {
            color : #DD0000;
        }

        p {
            color: #001188;
        }
    </style>
    <script src="../js/jquery/jquery.js"></script>
</head>
<body>
<script type="text/javascript">
<!--
    function changeTrack(play) {
       try {
           var audioElement = $("#AudioID");
           var domAudio = audioElement.get(0);
           var wasPlaying = !domAudio.paused;
           domAudio.currentTime = 0;
           $("#AudioSource").attr("src", play).attr("type","audio/mpeg");

           /****************/
           audioElement[0].pause();
           audioElement[0].load();//suspends and restores all audio element
           if (!wasPlaying) {
               domAudio.pause();
           } else {
               domAudio.play();
           }
       } catch (e) {

           $("#errorData").html(e.message)
       }
       return true;
    }

    window.onunload = function(){
     alert("stopping");
    }
//-->
</script>
<div style="text-align:center;">
    <form style="padding:0; margin:0;margin-bottom:5px;" onsubmit="return false;">
        <select onchange="changeTrack(this.value);" style="min-width:300px;" title="dummyNAME">
            <option value="http://radiopositief.live-streams.nl:80/live">Radio Positief Nederland [UKGR]</option>
            <option value="http://162.210.196.217:8002/stream.mp3">Rede Aleluia São Paulo Brasil</option>
            <option value="https://api.spreaker.com/v2/episodes/7970672/stream">Universal Church Radio Usa</option>
            <option value="http://192.99.18.164:9994/;.m3u">Iglesia Universal Nicaragua</option>
            <option value="http://americalavoz.radioiglesia.com:8066/stream">Radio Iglesia Paraguay</option>
            <option value="http://streamall.alsolnet.com:443/redaleluya">Radio Iglesia Argentina</option>
        </select>
    </form>
    &nbsp;<br />
    <div style="display:block;overflow:hidden;">
        <audio  id="AudioID" controls="controls">
            <source id="AudioSource" src="http://radiopositief.live-streams.nl:80/live" type="audio/mp4">
        </audio>
    </div>

    <span id="errorData">......&nbsp;</span>
</div>
</body>
</html>
