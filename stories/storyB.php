<?php
header("Cache-Control: no-store, no-cache, must-revalidate"); // HTTP/1.1
header("Cache-Control: post-check=0, pre-check=0", false);
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
header("Pragma: no-cache"); // HTTP/1.0
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Gezegende dag</title>
    <link href="../css/base.css" rel="stylesheet" />
</head>
<body>
    <h1>Geen vijand maar een Vriend</h1>
    <p><img class="story" src="http://ukgr.nl/helpcentre/wp-content/uploads/2016/05/ShadeliseWeb.jpg" alt="ShadeliseWeb"  />
    Ondanks dat ik niks tekort kwam en alles kreeg wat ik wou, was er toch een enorme leegte in mij die maar niet vervuld werd. Om dit op te vullen ging ik
        vaak uit en hoewel ik niet iemand was die dronk, probeerde ik het omdat ik dacht dat het mij misschien kon vervullen, maar ook dit hielp niet.</p>
    <p>Ik wist dat er iets was dat ik miste en dit zocht ik buiten. Ik kwam in een relatie terecht en kreeg toen ook alles, maar ook dit was niet genoeg. Ik deed alles om een oplossing te vinden voor mijn leegte, maar die oplossing leek er niet te zijn. Ik kwam wel een paar keer naar het UKGR Centrum omdat mijn moeder erheen ging, maar het enige wat ik dacht was dat God niks voor mij kon betekenen. Ik werd op jonge leeftijd moeder, maar zelfs mijn kind kon mijn leegte niet vervullen.</p>
    <p>Ik leek ontzettend gelukkig maar van binnen was ik leeg.</p>
    <p>Er was een moment waarop ik een dieptepunt bereikte in mijn leven en het niet meer zag zitten. Ik zat in de auto met mijn kind en ging met volle snelheid de weg op in de hoop dat er iets zou gebeuren en ik er niet meer zou zijn. Maar op datzelfde moment hield mijn verstand me tegen en stopte ik. Ik kreeg hoop in mij en wist niet waar dit vandaan kwam.</p>
    <p>Zo besloot ik naar het UKGR Centrum te komen. Ik werd goed opgevangen en geholpen. Nadat ik de gebedsketting gevolgd had, merkte ik dat mijn leven steeds weer beter begon te worden. Ik dacht God niet meer nodig te hebben en verliet de kerk.</p>
    <p>Ik wilde alles op mijn manier blijven doen, maar omdat mijn eigen manier niet werkte, en de situaties juist verergerden begon ik God de schuld van alles te geven en Hem als een vijand te zien.</p>
    <p>Ik merkte dat de problemen terugkwamen en erger waren dan voorheen.</p>
    <p>Ik wilde dit niet meer en besloot om mijn leven deze keer écht aan God te geven. Alles wat mij geadviseerd werd deed ik, ik volgde de gebedsketting op vrijdag voor de bevrijding en woonde de dienst op zondag bij voor de innerlijke vrede. Dit was niet makkelijk, maar na 11 maanden te hebben doorgezet kan ik vandaag de dag zeggen dat ik een totaal andere Shadelise ben. De persoon die ik voorheen was bestaat niet meer.</p>
    <p>Vandaag de dag ben ik gelukkig, de leegte die ik voorheen had is voorgoed verdwenen. Ik kan nu oprecht zeggen dat ik ware vrede heb, de innerlijke vrede. Ik zie God nu niet meer als een vijand, maar als mijn beste Vriend.</p>
    <p><strong>Shadelise Jones</strong></p>
</body>
</html>