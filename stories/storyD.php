<?php
header("Cache-Control: no-store, no-cache, must-revalidate"); // HTTP/1.1
header("Cache-Control: post-check=0, pre-check=0", false);
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
header("Pragma: no-cache"); // HTTP/1.0
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Gezegende dag</title>
    <link href="../css/base.css" rel="stylesheet" />
</head>
<body>
<h1>Ik was onzeker</h1>
<p><img class="story" src="http://ukgr.nl/helpcentre/wp-content/uploads/2016/09/OlettaJoao.jpg" alt="OlettaJoao"/>
    Het financiële leven van mijn gezin was niet goed, we hadden in in die tijd ook geen huis. We woonden 4 jaar lang samen in een huis met zigeuners uit Westers-Europese landen. Met een gezin van 6 mensen deelden we 2 kamers, waarbij ik in dezelfde kamer sliep als mijn broertjes.</p>
<p>Dit bracht voor mij veel frustraties en woede met zich mee, omdat ik begon te “puberen” en, net als ieder meisje van mijn leeftijd, privacy wilde hebben.</p>
<p>Mijn vader was de kostwinnaar. Mijn moeder was erg ziek en lag hierdoor vaak in het ziekenhuis, waardoor ik de zorg voor mijn broertjes op me nam. We waren altijd bang dat we onze moeder zouden verliezen.</p>
<p>Ik begon vaak ruzie te hebben thuis en ik groeide op met onzekerheden en een angst om fouten te maken. Ik wilde dat mensen mij perfect vonden, maar hoe ouder ik werd hoe onzekerder ik werd. Ik verhulde dit door een “masker” te dragen. Ik dacht dat ik altijd gelijk had en luisterde alleen maar naar mijn eigen gedachten. Bovendien ging het ook niet goed op school en voelde ik me minderwaardig en minder slim dan de anderen.</p>
<p>Doordat ik zo gesloten was dacht men op school dat er geen potentie in mij zat. Ik durfde geen vragen te stellen als ik iets niet wist, wat invloed had op mijn cijfers. Ik kon beter presteren, maar ik durfde niets van mijzelf te laten horen of te laten zien vanwege de onzekerheid.</p>
<p>Toen ik in het laatste jaar moest stoppen bereikte ik mijn dieptepunt. De angsten die ik had kwamen uit en de negatieve gedachtes over mijzelf werden door deze situatie “bevestigd”.</p>
<p>Door de onzekerheid kwam ik ook niet aan een baan, omdat ik niet durfde te solliciteren en wanneer ik dit wel deed werd ik afgewezen.</p>
<p>Ik kwam al naar het UKGR Centrum met mijn ouders, maar door de jaren heen praktiseerde ik niet wat ik hoorde. Pas na 8 jaar in het UKGR Centrum te zijn, kwam ben ik echt gaan praktiseren wat ik hoorde, omdat ik wilde veranderen. Ik begon mijn geloof te gebruiken en begon de dingen te doen die ik normaal gesproken door de onzekerheid niet zou durven. Ik nam ook deel aan voorstellen van geloof en na een tijd bemerkte ik de veranderingen in mijzelf.</p>
<p>Vandaag de dag ben ik niet meer onzeker. Nadat ik dit had overwonnen werd ik voor dezelfde opleiding en hetzelfde niveau aangenomen op een andere school en dit gaat goed. Ik haal goede cijfers en durf nu ook vragen te stellen wanneer ik iets niet weet. Ik veranderde van binnen en daardoor veranderde de situatie van buiten.</p>
<p>Nu heb ik een baan en doe ik dit werk met plezier.</p>
<p>Ik heb vrede en blijdschap en ben trots op wie ik nu ben.</p>
<p><strong>Oletta João</strong></p>


</body>
</html>