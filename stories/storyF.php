<?php
header("Cache-Control: no-store, no-cache, must-revalidate"); // HTTP/1.1
header("Cache-Control: post-check=0, pre-check=0", false);
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
header("Pragma: no-cache"); // HTTP/1.0
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Gezegende dag</title>
    <link href="../css/base.css" rel="stylesheet" />
</head>
<body>

<h1>De ziektes overwonnen</h1>
<p><img class="story" src="http://ukgr.nl/helpcentre/wp-content/uploads/2017/01/MoniqueFleur-300x248.jpg"  alt="MoniqueFleur" />
    Sinds mijn geboorte ben ik altijd ziek geweest. 48 jaar leed ik aan hevige astma. Ik liep altijd rond met inhalers en kreeg zelfs zware medicijnen zoals Prednizon voorgeschreven. Daarnaast leed ik ook nog eens 38 jaar aan bloedarmoede, waarbij ik een hele lage bloedwaarde had, had ik 18 jaar lang bekkeninstabiliteit en leed ook nog eens aan reumatische klachten.</p>
<p>Ik wist niet wat gezond zijn betekende, want heel mijn leven was ik ziek en leed ik aan pijnen. Mijn huis zat vol medicijnen, maar desondanks rolde ik vaak door mijn huis van de pijn of het gebrek aan zuurstof.</p>
<p>Een familielid van mijn man die naar de UKGR komt nodigde ons uit om ook te komen. Hij vertelde ons dat er een oplossing bestond voor mijn situatie. Toen ik voor de eerste keer kwam, trok de kracht die daar was mij meteen aan. Ik hoorde ook hoe mensen vertelden dat zij genezen werden van hun klachten. Dat wilde ik ook. Ik begon dus met al mijn kracht te doen wat daar geleerd werd.</p>
<p>Na een verloop van tijd zei mijn man opeens tegen mij dat ik al enige tijd geen inhaler of pijnstillers aan het gebruiken was. Toen ik erbij stil stond, zag ik inderdaad in dat ik al heel lang niets gebruikte. Voorheen moest ik elke dag verschillende inhalers gebruiken, maar nu niet meer.</p>
<p>Ik ben een medisch wonder, want al mijn ongeneeslijke ziekten en klachten zijn nu overwonnen. Ik heb geen astma, bloedarmoede of reumatische klachten meer, mijn bekken zijn stabiel. Nu weet ik wat gezond en sterk zijn betekent.</p>
<p><strong>Monique Fleur</strong></p>

</body>
</html>
