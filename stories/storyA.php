<?php
header("Cache-Control: no-store, no-cache, must-revalidate"); // HTTP/1.1
header("Cache-Control: post-check=0, pre-check=0", false);
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
header("Pragma: no-cache"); // HTTP/1.0
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Gezegende dag</title>
    <link href="../css/base.css" rel="stylesheet" />
</head>
<body>
  <h1>Mijn leven hoefde inderdaad niet zo te zijn</h1>
  <p>
      <img class="story" src="http://ukgr.nl/helpcentre/wp-content/uploads/2016/06/webgetjoan.jpg" alt="webgetjoan" />
      Mijn leven was totaal vernietigd. Ik was depressief, rookte drie pakken sigaretten per dag, was heel angstig en kon niet goed slapen. Wanneer ik slapeloze nachten had, had ik zo nu en dan een jointje nodig om te kunnen slapen.
      Bovendien was ik ook erg agressief vanwege woede die ik in mij had. Ik was altijd bereid om te vechten, wanneer iemand naar mijn mening al te lang naar mij of mijn kinderen keek, wilde ik al ruzie zoeken.</p>
  <p>Ik voelde me heel eenzaam en had zelfmoordneigingen. Ik wilde niet meer leven en had ook niet het gevoel dat er iemand voor mij klaarstond, waardoor mijn manier van kijken steeds negatiever werd. Ik haatte de wereld en zowel geestelijk als fysiek ging het niet goed.</p>
  <p>Na tevergeefs bepaalde vormen van spirituele hulp te hebben gezocht, ben ik op uitnodiging van iemand in het UKGR Centrum terecht gekomen, waar ik leerde dat mijn leven niet zo hoefde te zijn. Ik zette in praktijk wat mij daar werd geadviseerd en begon de gebedskettingen op vrijdag te volgen en de diensten op zondag trouw bij te wonen.</p>
  <p>Na een paar maanden had ik geen last meer van slapeloosheid of angstgevoelens meer. Na deze veranderingen te hebben gezien raakte ik gemotiveerd om ook de oplossing voor mijn andere problemen te zoeken. Ik bleef de gebedskettingen volgen en nam deel aan voorstellen en campagnes van geloof waarna ik ook veranderingen begon te zien.</p>
  <p>Vandaag is mijn leven totaal anders. Ik slaap goed, heb geen angstgevoelens meer, heb geen sigaretten of andere middelen meer nodig om mij goed te voelen en ben niet meer depressief.</p>
  <p>Wat ik in de UKGR heb gehoord klopte helemaal, Mijn leven hoefde inderdaad niet zo te zijn en ik ervaar nu hoe goed het werkelijk kan zijn. Ik ben gelukkig.</p>
  <p><strong>Joan Crawford</strong></p>
</body>
</html>
