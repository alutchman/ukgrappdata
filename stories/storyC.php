<?php
header("Cache-Control: no-store, no-cache, must-revalidate"); // HTTP/1.1
header("Cache-Control: post-check=0, pre-check=0", false);
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
header("Pragma: no-cache"); // HTTP/1.0
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Gezegende dag</title>
    <link href="../css/base.css" rel="stylesheet" />
</head>
<body>
<h1>Genezen en gelukkig</h1>
<p><img class="story" src="http://ukgr.nl/helpcentre/wp-content/uploads/2015/11/Get_Website_SandraWinklaar.jpg" alt="Get_Website_SandraWinklaar" />
    20 jaar lang maakten slapeloosheid, depressie en verdriet deel uit van mijn dagelijks leven. Dit alles begon nadat er een eind kwam aan mijn huwelijk, dat tien jaar duurde. Ik was onzeker, boos en ging liever niet met mensen om.</p>
<p>Daarnaast had ik ook maagklachten. Ik kreeg medicijnen, maar die hielpen tijdelijk en de pijn keerde steeds terug. Volgens de arts was het chronisch.</p>
<p>In het UKGR Centrum leerde ik dat er een oplossing voor mijn situatie bestond en hoe ik die kon bereiken. Ik ontving adviezen, volgde de gebedskettingen op dinsdag en vrijdag, daarnaast nam ik deel aan de diensten op zondag voor de innerlijke versterking.</p>
<p>Na ongeveer drie maanden merkte ik dat de maagklachten weg waren, na 3 á 4 maanden kon ik al beter slapen en na ongeveer 6 maanden was ik niet meer depressief.</p>
<p>Vandaag de dag ben ik niet meer verdrietig of depressief, ik slaap goed en ben genezen van de maagklachten. Nu maken blijdschap en vrede deel uit van mijn dagelijks leven.</p>
<p><strong>Sandra Winklaar</strong></p>
</body>
</html>