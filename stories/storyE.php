<?php
header("Cache-Control: no-store, no-cache, must-revalidate"); // HTTP/1.1
header("Cache-Control: post-check=0, pre-check=0", false);
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
header("Pragma: no-cache"); // HTTP/1.0
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Gezegende dag</title>
    <link href="../css/base.css" rel="stylesheet" />
</head>
<body>
<h1>Niet meer verslaafd</h1>
<p> <img class="story" src="http://ukgr.nl/helpcentre/wp-content/uploads/2016/03/Minguel.jpg" alt="Minguel" />
    Ik begon op mijn 20e harddrugs te gebruiken en heb 25 jaar lang cocaïne gebruikt. Daarnaast was ik ook verslaafd aan sigaretten en alcohol. Het was een combinatie die mijn leven ruïneerde. Door mijn verslaving verloor ik baan na baan en ging mijn leven nergens naartoe. Ik wilde er zolang vanaf komen, maar het lukte mij niet, hoe vaak ik het ook probeerde.</p>
<p>Ondanks dat mijn leven een ramp was, wilde ik niet ingaan op uitnodigingen om naar de kerk te gaan. Ik dacht dat dat niet nodig was voor mij.</p>
<p>Maar op een dag werd het toch te veel voor mij en besloot ik om een kerk te zoeken, omdat ik inzag dat ik hulp nodig had. Via een lid van de UKGR kwam ik hier terecht. Toen ik in het UKGR Centrum kwam, sprak ik met iemand die ook verslaafd was en door de kracht van het geloof en God had hij dat probleem overwonnen. Hij vertelde mij om te blijven volharden en dan zou ik ook mijn probleem kunnen overwinnen. Maar na de dienst ging ik toch weer drugs kopen. Ik was eraan gebonden.</p>
<p>Ondanks het feit dat ik toch drugs had gekocht en gebruikt, bleef ik toch teruggaan naar de kerk. Ik bleef volharden, gebed van pastors ontvangen, counseling krijgen en getuigenissen zien.</p>
<p>Ik volgde meerdere gebedskettingen op vrijdag, waar ik leerde wat de bron van mijn probleem was. Langzamerhand begon mijn drugsgebruik af te nemen. Maar het duurde langer dan een jaar om ervan af te komen. Mijn volharding en geloof in God waren echter groter dan mijn verslaving. Vandaag de dag gebruik ik alleen nog maar mijn geloof en niks anders. Mijn leven is veranderd, ik heb stabiliteit, vrede en ben werkelijk gelukkig.</p>
<p><strong>Rignald Minguel</strong></p>

</body>
</html>
