<?php
header("Cache-Control: no-store, no-cache, must-revalidate"); // HTTP/1.1
header("Cache-Control: post-check=0, pre-check=0", false);
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
header("Pragma: no-cache"); // HTTP/1.0
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Gezegende dag</title>
    <link href="css/base.css" rel="stylesheet" />
</head>
<body>

<h1>Wat we doen</h1>
<p>
We bieden veel diensten aan, die ook nog eens kosteloos zijn, aan een ieder die wil. 
Ongeacht leeftijd, afkomst of religie. lectuur, seminars en bijeenkomsten om over 
relevante onderwerpen te onderwijzen op een praktische manier, 
door geestelijke en praktische adviezen te geven en door middel van gebed.
</p>
<h1>Counseling en InfoLijn 24 uur</h1>
<p>
Onze InfoLijn is elke dag van de week, het hele jaar door, 24 uur per dag beschikbaar. 
Als u last heeft van een ernstig probleem, het gevoel heeft dat niemand om u geeft, 
en iemand nodig heeft om uw hart bij te luchten, of als u niet alleen gehoord 
wilt worden, maar ook praktisch advies wilt ontvangen over hoe u uw specifieke 
situatie aan kunt pakken, dan kunnen wij u helpen.
</p>
<p>
Het maakt niet uit wie u bent, wat u hebt gedaan, uw geloof, religie of afkomst, 
het belangrijkste is dat uw oproep beantwoord wordt door iemand die 
echt om u geeft en u wil helpen.
</p>

<p>
Zie ons als een betrouwbare vriend die zijn best zal doen om te helpen. 
Degene die met u zal praten zal u goed kunnen begrijpen, omdat hij/zij 
ook problematische ervaringen heeft meegemaakt, maar die wist te overwinnen 
na de steun die hij/zij  ontving toen die persoon op een dag ook naar het UKGR Centrum kwam.
</p>

<p>
Dus wanneer iemand ons belt, voelen we echt de pijn van die persoon. 
Wij dwingen niemand ergens toe. Als alles wat u nodig hebt, 
iemand om te luisteren is, dan zullen we dat bieden. Als u meer hulp zou 
willen ontvangen, dan zullen we u naar het UKGR Centrum bij u in de buurt 
begeleiden! We zijn 24 uur per dag beschikbaar, op: <strong style="color:#EE0000">070 388 84 42</strong>.
</p>

<h1>Wat wij geloven</h1>
<ul style="margin-bottom:20px;">
<li><strong>Wij geloven</strong> dat het Oude en Nieuwe Testament volledig zijn geïnspireerd door 
God en zien hen als enige ware leefregels voor het leven en het geloof</li> 
<li><strong>Wij geloven</strong> in één enkele God die voor eeuwig bestaat in drie personen – Vader, Zoon en Heilige Geest; 
<li><strong>Wij geloven</strong> dat Jezus Christus werd verwekt door de Heilige Geest in de maagd Maria en 
waarlijk God is en een waarlijk Mens; 
<li><strong>Wij geloven</strong> dat God de mens schiep naar Zijn eigen beeld: Dat de mens zondigde en 
hierdoor de lichamelijke en geestelijke doodstraf op zich bracht en dat alle mensen een zondige aard beërven; 
<li><strong>Wij geloven</strong> dat de Here Jezus als een plaatsvervangend offer voor ons stierf en dat allen die 
in Hem geloven, gerechtvaardigd zijn door Zijn vergoten bloed; 
<li><strong>Wij geloven</strong> in de opwekking van de Here Jezus, Zijn hemelvaart en Zijn tegenwoordige 
leven als onze Hogepriester en Middelaar; 
<li><strong>Wij geloven</strong> in de persoonlijke terugkomst van de Here Jezus Christus in heerlijkheid; 
<li><strong>Wij geloven</strong> dat wie zich van hun zonden berouwen en zich door het geloof aan de Here
 Jezus Christus vasthouden, wedergeboren worden door de Heilige Geest en kinderen van God worden; 
<li><strong>Wij geloven</strong> in de doop met de Heilige Geest die de gelovigen versterkt voor de bediening, 
gevolgd door bovennatuurlijke gaven van de Heilige Geest; 
<li><strong>Wij geloven</strong> in de aan God gewijde bediening van apostel, profeet, evangelist, pastor en leraar; 
<li><strong>Wij geloven</strong> in de opstanding en het eeuwig leven voor de rechtvaardigen en in de opstanding 
en het oordeel voor de onrechtvaardigen; 
<li><strong>Wij geloven</strong> dat de ware Kerk bestaat uit hen die zijn vrijgekocht door de Here Jezus en herboren
 zijn uit de Heilige Geest: Dat de Kerk hier op aarde het karakter zou moeten overnemen van de eerste Kerk; 
<li><strong>Wij geloven</strong> dat de Here Jezus twee bevelen gaf: De doop in het water en het Heilig Avondmaal 
om in acht te nemen als handelingen van gehoorzaamheid en als een voortdurende getuigenis van het Christelijk geloof; 
<li><strong>Wij geloven</strong> dat wonderbaarlijke genezing onderdeel is van het Evangelie van heden net zoals in het verleden; 
<li><strong>Wij geloven</strong> dat de Bijbel ons leert dat zonder heiliging, geen mens God kan aanschouwen; </li>
<li><strong>Wij geloven</strong> dat geloof zonder werken een dood geloof is en verzorgen daarom ook sociaal werk. </li>
</ul>


</body>
</html>