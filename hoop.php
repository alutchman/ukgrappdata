<?php
header("Cache-Control: no-store, no-cache, must-revalidate"); // HTTP/1.1
header("Cache-Control: post-check=0, pre-check=0", false);
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
header("Pragma: no-cache"); // HTTP/1.0
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Zekerheid en Zegen</title>
    <link href="css/base.css" rel="stylesheet" />
</head>
<body>

<article>
    <section >
        Velen van ons gaan tijdens het leven door moeilijke tijden. Sommige zien het niet meer zitten en weer anderen
        leven als zombies, zonder enige vooruitgang.
    </section>
    &nbsp;<br />
    <section >
        U bent niet alleen. Velen hebben hetzelfde pad bewandeld waarvan u nu vindt dat het onhoudbaar is.
        Allen hebben het kunnen doorbreken door volharding en ook u zal het kunnen doorbreken.
    </section>
    &nbsp;<br />
</article>
<div style="text-align: center;">
    <iframe width="95%" height="250" src="https://www.youtube-nocookie.com/embed/xPtJjcHn7P0?rel=0" frameborder="0" allowfullscreen></iframe>
</div>
&nbsp;<br />
<article>
    Toch is er meer moois en goeds waar u van kunt genieten, als u
    zichzelf daarvoor openstelt. Zodra wij beseffen dat onze wil ons kan helpen en onze geloof ons zekerheid biedt,
    is het leven een zegen.
</article>
&nbsp;<br />
<div style="text-align: center;">
    <iframe width="96%" height="300" src="https://www.youtube-nocookie.com/embed/vD8RnvE4kl8?rel=0" frameborder="0" allowfullscreen></iframe>
</div>


<marquee scrollamount="3" style="margin:0 0 0 5px; padding :0;">
    <p  style="margin:0; padding :0;">
        <span style="color:firebrick;padding-left:100px;">Geloof is zekerheid
        &nbsp;&nbsp;&nbsp;
        <span style="color:seagreen">Onze wil is sterker dan de problemen waarmee we te maken krijgen. </span>
        &nbsp;&nbsp;&nbsp;
        <span style="color:rebeccapurple">U zult doorbreken door volharding.</span>
        &nbsp;&nbsp;&nbsp;&nbsp;
    </p>
</marquee>

<div style="text-align: center;">
    <iframe width="96%" height="300" src="https://www.youtube-nocookie.com/embed/mnjiOeo0Ccs?rel=0" frameborder="0" allowfullscreen></iframe>
</div>
<div style="text-align: center;">
    <iframe  width="96%" height="300" src="https://www.youtube-nocookie.com/embed/qpKCZYl3Pn0?rel=0" frameborder="0" allowfullscreen></iframe>
</div>
<footer>
    <div style="color:seagreen;float:right;">email: <a href="mailto:info@ukgr.nl">info@ukgr.nl</a></div>
    <div style="color:seagreen;float:left;">&#9742; : <a href="tel:+31703888442">070 3888442</a></div>
    <div>&nbsp;</div>
</footer>
</body>
</html>
</html>