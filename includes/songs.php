<?php
header("Cache-Control: no-store, no-cache, must-revalidate"); // HTTP/1.1
header("Cache-Control: post-check=0, pre-check=0", false);
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
header("Pragma: no-cache"); // HTTP/1.0
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Gezegende dag</title>
    <link href="../css/base.css" rel="stylesheet" />
</head>
<body>

<h2 class="logo">Als een hert dat verlangt</h2>

<div style="display:block;overflow:hidden;text-align:center;">
    <audio controls>
        <source src="../media/Als_een_hert_dat_verlangt_naar_water.mp3" type="audio/mpeg">
        Your browser does not support the audio element.
    </audio>
</div>

<article class="songs">
    <section >
        Als een hert dat verlangt naar water,&nbsp;</br />
        zo verlangt mijn ziel naar U.&nbsp;</br />
        U alleen kunt mijn hart vervullen,&nbsp;</br />
        mijn aanbidding is voor U.&nbsp;</br />
    </section>
    &nbsp;<br />
    <section>
        U alleen bent mijn Kracht, mijn Schild.&nbsp;</br />
        Aan U alleen geef ik mij geheel.&nbsp;</br />
        U alleen kunt mijn hart vervullen,&nbsp;</br />
        mijn aanbidding is voor U.&nbsp;</br />

    </section>
</article>
<div style="display:block;overflow:hidden;clear: both;">

    <div style="text-align:center;">





    </div>
</div>


</body>
</html>




