<?php
header("Cache-Control: no-store, no-cache, must-revalidate"); // HTTP/1.1
header("Cache-Control: post-check=0, pre-check=0", false);
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
header("Pragma: no-cache"); // HTTP/1.0
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Gezegende dag</title>
    <link href="../css/base.css" rel="stylesheet" />
</head>
<body>

<h1 class="blueToGold">Voetstappen in het zand</h1>
<div style="display:block;overflow:hidden;">
    <p>
        <img class="story" src="../images/voetstappen-in-het-zand.jpg"  alt="Voetspappen Zand" />
        Ik droomde eens en zie
        ik liep aan ‘t strand bij lage tij.
        Ik was daar niet alleen,
        want ook de Heer liep aan mijn zij.
    </p>
</div>

<p >
    We liepen samen het leven door,
    en lieten in het zand,
    een spoor van stappen; twee aan twee,
    de Heer liep aan mijn hand.
</p>
<p >
    Ik stopte en keek achter mij,
    en zag mijn levensloop,
    in tijden van geluk en vreugde,
    van diepe smart en hoop.
</p>
<p>

    Maar als ik het spoor goed bekeek,
    zag ik langs heel de baan,
    daar waar het juist het moeilijkst was,
    maar één paar stappen staan.
</p>
<p>

    Ik zei toen “Heer waarom dan toch?
    Juist toen ik U nodig had,
    juist toen ik zelf geen uitkomst zag,
    op het zwaarste deel van mijn pad…”
</p>
<p>
    <img class="story" src="../images/voetstappen-in-het-zandB.jpg"  alt="Voetspappen Zand" />
    De Heer keek toen vol liefde mij aan,
    en antwoordde op mijn vragen;
    “Mijn lieve kind, toen het moeilijk was,
    toen heb ik jou gedragen…”
</p>

</body>
</html>




