<?php
header("Cache-Control: no-store, no-cache, must-revalidate"); // HTTP/1.1
header("Cache-Control: post-check=0, pre-check=0", false);
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
header("Pragma: no-cache"); // HTTP/1.0
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
?>
<div><span style="float:right;">10:00 am</span><h2>Innerlijke Vrede</h2></div>
<p>
Voordat een persoon vrede van buiten kan hebben, dient hij vrede in zichzelf te hebben.
“Vrede laat Ik u, mijn vrede geef Ik u…” (Johannes 14:27).
</p>
<p>
In het dagelijks leven zijn er veel dingen die ons bezorgd kunnen maken, zoals nare situaties 
op de werkvloer, werkloosheid, onrust in het huwelijk of in het gezin, 
gezondheidsklachten, stress enzovoorts.
</p>
<p>
Dit zijn een aantal problemen waar wij tegen strijden. 
Maar er is één ding dat wij allemaal nodig hebben om onze 
problemen en strijden te overwinnen. Innerlijke Vrede!
</p>
<p>
U bent van harte uitgenodigd om samen met ons deze Innerlijke Vrede te zoeken en te ontvangen.
</p>
<p>
“Vrede laat Ik u, mijn vrede geef Ik u…” (Johannes 14:27).
</p>
<video controls="true" width="100%">
    <source src="http://ukgr.nl/UKGRWEEK.MP4" type="video/mp4" />
</video>

<div><span style="float:right;">15:00</span><h2>Middag van Kracht</h2></div>
<p>Het is zo sterk dat het, het begin van de verandering van jouw leven kan teweegbrengen.
Wat is dit voor een kracht? Dat zal je leren en ontdekken tijdens deze bijeenkomst.
</p>
<div><span style="float:right;">18:00</span><h2>Studie van de Openbaring</h2></div>
<p>Ontdek de geheimen van de toekomstige gebeurtenissen en werkt aan uw geestelijk leven. 
</p>
