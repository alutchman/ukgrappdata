<?php
header("Cache-Control: no-store, no-cache, must-revalidate"); // HTTP/1.1
header("Cache-Control: post-check=0, pre-check=0", false);
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
header("Pragma: no-cache"); // HTTP/1.0
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
?>
<!doctype html>
<html lang="us">
<head>
	<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, ">
	<title>UKGR Agenda</title>
	<link href="css/jquery-ui.css" rel="stylesheet" />
    <link href="css/base.css" rel="stylesheet" />
	<style>


		div ul li {
		  font-size : 9pt;
		}
		
		
		.ui-tabs-anchor:active, .ui-tabs-anchor:focus{
		     outline:none;
		}​
	</style>
</head>
<body>

<h1>Agenda</h1>
<div id="tabs">
	<ul>
		<li><a href="includes/agenda/zo.php">Zo </a></li>
		<li><a href="includes/agenda/ma.html">Ma </a></li>
		<li><a href="includes/agenda/di.html">Di </a></li>
		<li><a href="includes/agenda/wo.html">Wo </a></li>
		<li><a href="includes/agenda/do.html">Do </a></li>
		<li><a href="includes/agenda/vr.html">Vr</a></li>
		<li><a href="includes/agenda/za.php">Za </a></li>
	</ul>



</div>


<script src="js/jquery/jquery.js"></script>
<script src="js/jquery-ui.js"></script>
<script>
var today = new Date();
var dagNummer = today.getDay();

var availableTags = [
	"ActionScript",
	"AppleScript",
	"Asp",
	"BASIC",
	"C",
	"C++",
	"Clojure",
	"COBOL",
	"ColdFusion",
	"Erlang",
	"Fortran",
	"Groovy",
	"Haskell",
	"Java",
	"JavaScript",
	"Lisp",
	"Perl",
	"PHP",
	"Python",
	"Ruby",
	"Scala",
	"Scheme"
];
$( "#autocomplete" ).autocomplete({
	source: availableTags
});


$( "#tabs" ).tabs({
    beforeLoad: function( event, ui ) {
        ui.jqXHR.error( function(e) {
            ui.panel.html( "Missing or defective resource: "+ ui.ajaxSettings.url);
        });
    },
    load: function() {
     
     },
    active:dagNummer
});


</script>
</body>
</html>
